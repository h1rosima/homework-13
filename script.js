/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

document.addEventListener('DOMContentLoaded', function () {
    const tabs = document.querySelector('.tabs');
    const tabContents = document.querySelectorAll('.tabs-content-text');

    tabs.addEventListener('click', function (event) {
        const clickedTab = event.target;
        if (clickedTab.classList.contains('tabs-title')) {
            const tabName = clickedTab.getAttribute('data-tabs');

            tabContents.forEach(function (tabContent) {
                if (tabContent.getAttribute('data-tabs') === tabName) {
                    tabContent.style.display = 'block';
                } else {
                    tabContent.style.display = 'none';
                }
            });

            const tabTitles = document.querySelectorAll('.tabs-title');
            tabTitles.forEach(function (title) {
                if (title.getAttribute('data-tabs') === tabName) {
                    title.classList.add('active');
                } else {
                    title.classList.remove('active');
                }
            });
        }
    });

    tabContents.forEach(function (tabContent) {
        if (!tabContent.classList.contains('active')) {
            tabContent.style.display = 'none';
        }
    });
});
